import numpy as np
from numpy.linalg import inv
import sklearn
import GPy

def matrix_kernel (X, x_scale=-1):
    
    if x_scale == -1:
        X_med = X[:]
        x_scale = np.median(np.sqrt(np.sum(np.power(X_med[:, :, np.newaxis].T - X_med[:, :, np.newaxis],2), axis=1))[np.triu_indices(len(X_med), 1)])
   
    kern_x = GPy.kern.RBF(input_dim=X.shape[1], lengthscale=x_scale)
    K = kern_x.K(X)
   
    return K

def var_norms (x, y, reg=None):
   
    n = len(x)
    if reg is None:
        reg = 1. / np.sqrt(n)
   
    L = matrix_kernel(y)
    K = matrix_kernel(x)
    W = K + reg*np.eye(n)
    solved = np.linalg.solve(W, K)
   
    return np.var(np.sqrt(np.diag(solved.T * L * solved)))

def adddirect(size):
    x = np.random.randn(size,1)
    n_y = np.random.randn(size, 1)
    y = (np.sin(10*x) + np.exp(3*x)) + n_y
    return x,y

# KCDC (https://arxiv.org/pdf/1804.04622.pdf) then outputs X-->Y in the following way: 


if __name__ == '__main__':
    # Generate data
    x,y = adddirect(1000)
    
    # Check causal relationship
    if var_norms(x,y) < var_norms(y,x):
                print("X ---> Y")
    else:       
                print("X <--- Y")